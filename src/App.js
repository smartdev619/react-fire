import React, { Component } from 'react';
import Home from './components/home'
import Friends from './components/friends'
import Login from './components/login'
import Register from './components/register'
import Profile from './components/profile'
import EditProfile from './components/profile/edit_profile'
import Header from './components/common/header'
import history from './history'
import {
  Router,
  Route
} from 'react-router-dom'
import * as firebase from 'firebase';
var config = {
  apiKey: "AIzaSyD3JGoXk0d3XusLdkYtTbKNH2y-Ueo6Q90",
  authDomain: "wallapp-e1b08.firebaseapp.com",
  databaseURL: "https://wallapp-e1b08.firebaseio.com",
  projectId: "wallapp-e1b08",
  storageBucket: "wallapp-e1b08.appspot.com",
  messagingSenderId: "60378528649"
  };
  firebase.initializeApp(config);
class App extends Component {
  render() {
    return (
      <div className="App">
      <Router history={history}>
          <div className="width">
            <Header/>
            <Route exact path="/" component={Home}/>
            <Route path="/friends" component={Friends}/>
            <Route path="/login" component={Login}/>
            <Route path="/register" component={Register}/>
            <Route path="/profile" component={Profile}/>
            <Route path="/editprofile" component={EditProfile}/>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
