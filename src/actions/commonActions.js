import * as firebase from "firebase";

export function uploadPicture(file){
    var storageRef = firebase.storage().ref();
    var ref = storageRef.child('images/mountains.jpg');
    return ref.put(file).then(function(snapshot) {
        return new Promise((resolve, reject) => resolve(snapshot));
    });
}