import * as firebase from "firebase";
import { asyncActionNames, buildAsyncActions } from "../GlobalActionCreators";
// creating action names and action creator
const getLoginName = asyncActionNames("LOGIN_USER");
const getLoginNameCreator = buildAsyncActions(getLoginName);

export function login(email, password) {
  return function(dispatch) {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(result => {
          dispatch(getLoginNameCreator.success(result));
          resolve(result);
        })
        .catch(function(error) {
          reject(error.message);
          console.log(error);
        });
    });
  };
}

export function logout() {
  return function(dispatch) {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .signOut()
        .then(function() {
          dispatch(getLoginNameCreator.success(false));
          resolve("logout");
        })
        .catch(function(error) {
          reject(error.message);
        });
    });
  };
}

export function checkUserLogin() {
  return function(dispatch) {
    return new Promise((resolve, reject) => {
      firebase.auth().onAuthStateChanged(function(user) {
        dispatch(getLoginNameCreator.success(user));
        resolve(user);
      });
    });
  };
}

export function updateUser(userdata) {
  return function(dispatch) {
    return new Promise((resolve, reject) => {
      var user = firebase.auth().currentUser;

      user
        .updateProfile(userdata)
        .then(function(user) {
          dispatch(
            getLoginNameCreator.success(firebase.auth().currentUser)
          );
          resolve(user);
        })
        .catch(function(error) {
          console.log(error);
        });
    });
  };
}
