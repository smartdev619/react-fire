import React, { Component } from "react";
import AppBar from "material-ui/AppBar";
import Toolbar from "material-ui/Toolbar";
import Typography from "material-ui/Typography";
import Button from "material-ui/Button";
import IconButton from "material-ui/IconButton";
import MenuIcon from "material-ui-icons/Menu";
import ChevronRightIcon from "material-ui-icons/ChevronRight";
import Drawer from "material-ui/Drawer";
import Divider from "material-ui/Divider";
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import * as logoutAction from '../../actions/loginAction';
import history from '../../history'
import { Link } from "react-router-dom";

let self;
class Header extends Component {
  constructor() {
    super();
    self = this;
    this.state = {
      open: false,
      anchor: "left",
      user: false
    };
  }

  componentWillMount() {
    this.props.actions.userAction.checkUserLogin().then((user)=>{
      self.setState({ user });
    })
  }

  componentWillReceiveProps(nextprops){
    self.setState({ user : nextprops.user});
  }

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  userLogout = () => {
    this.props.actions.userAction.logout().then((result) => {
      history.push('/login')
    })
    .catch(function(error) {
      console.log(error)
    });
  };

  render() {
    const { anchor, open } = this.state;

    const drawer = (
      <Drawer anchor={anchor} open={open}>
        <div>
          <div>
            <IconButton onClick={this.handleDrawerClose}>
              <ChevronRightIcon />
            </IconButton>
          </div>
          <Divider />
        </div>
      </Drawer>
    );

    let before = null;
    let after = null;

    if (anchor === "left") {
      before = drawer;
    } else {
      after = drawer;
    }

    return (
      <div>
        <AppBar position="static">
          <Toolbar>
            <IconButton
              style={{ marginLeft: -12, marginRight: 20 }}
              color="contrast"
              aria-label="Menu"
            >
              <MenuIcon onClick={this.handleDrawerOpen.bind(this)} />
            </IconButton>
            <Typography type="title" color="inherit" style={{ flex: 1 }}>
              Title
            </Typography>
            {this.props.user || this.state.user? (
              <div>
<Link to="/friends">
    <Button color="contrast">Friends</Button>
  </Link>

              {this.state.user?
                <Link to="/profile">
                  <Button color="contrast">{this.state.user.email}</Button>
                </Link>:''
}
                <Button color="contrast" onClick={this.userLogout.bind(this)}>
                  Logout
                </Button>
              </div>
            ) : (
              <div>
                <Link to="/login">
                  <Button color="contrast">Login</Button>
                </Link>
                <Link to="/register">
                  <Button color="contrast">Register</Button>
                </Link>
              </div>
            )}
          </Toolbar>
        </AppBar>
        {before}
        {after}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.LoginReducer.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      userAction: bindActionCreators(logoutAction, dispatch),
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Header);
