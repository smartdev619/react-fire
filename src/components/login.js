import React, { Component } from "react";
import Button from "material-ui/Button";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as loginAction from "../actions/loginAction";
import { toast,ToastContainer } from 'react-toastify';

class Login extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  state = {
    email: "",
    password: "",
    disabled: true
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });

    if (this.state.email !== "" && this.state.password !== "") {
      this.setState({ disabled: false });
    }
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.actions.loginUser
      .login(this.state.email, this.state.password)
      .then(result => {
        this.props.history.push("/profile");
      })
      .catch(function(error) {
        toast.error(error);
      });
  };
  render() {
    return (
      <div id="login-page" className="row">
      <ToastContainer />
      
      <div className="col s12">
      <form
          noValidate
          autoComplete="off"
          onSubmit={this.handleSubmit.bind(this)}
          className="login-form"
        >
          <div className="row">
            <div className="input-field col s12 center">
              <img src="../../images/logo/login-logo.png" alt="" className="circle responsive-img valign profile-image-login"/>
              <p className="center login-form-text">Material Design Admin Template</p>
            </div>
          </div>
          <div className="row margin">
            <div className="input-field col s12">
              <i className="material-icons prefix pt-5">person_outline</i>
              <input
                id="email"
                value={this.state.email}
                onChange={this.handleChange("email")}
                type="email"
              />
              <label className="center-align">Email</label>
            </div>
          </div>
          <div className="row margin">
            <div className="input-field col s12">
              <i className="material-icons prefix pt-5">lock_outline</i>
              <input
                id="password"
                value={this.state.password}
                onChange={this.handleChange("password")}
                type="password"
              />
              <label>Password</label>
            </div>
          </div>
          <div className="row">
            <div className="input-field col s12">
            <Button type="submit" raised disabled={this.state.disabled}>
            Login
          </Button>
            </div>
          </div>
        </form>
    </div>
        
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state
  };
}
function mapDispatchToProps(dispatch) {
  return {
    actions: {
      loginUser: bindActionCreators(loginAction, dispatch)
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
