import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Edit from "material-ui-icons/Edit";
class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null
    };
  }

  componentWillMount(){
    if(this.props.user){
      this.setState({user:this.props.user.providerData[0]})
    }
    
  }

  componentWillReceiveProps(nextprops){
    if(nextprops.user){
    this.setState({user:nextprops.user.providerData[0]})
    }
  }
  render() {
    if(!this.state.user){
      return(
        <div>Loading</div>
      )
    }else{
      return (
        <div className="Home">
          {this.state.user !== null ? (
            <section id="content">
              <div className="container">
                <div id="profile-page" className="section">
                  <div id="profile-page-header" className="card">
                    <div className="card-image waves-effect waves-block waves-light">
                      <img
                        className="activator"
                        src="https://cdn.pixabay.com/photo/2015/12/13/09/42/banner-1090835_960_720.jpg"
                        alt="user background"
                      />
                    </div>
                    <figure className="card-profile-image">
                      <img
                        src={this.state.user.photoURL}
                        alt="profile"
                        className="circle z-depth-2 responsive-img activator"
                      />
                    </figure>
                    <div className="card-content">
                      <div className="row">
                        <div className="col s3 offset-s2">
                          <h4 className="card-title grey-text text-darken-4">
                          {this.state.user.displayName}
                          </h4>
                          <p className="medium-small grey-text">
                            Project Manager
                          </p>
                        </div>
                        <div className="col s2 center-align">
                          <h4 className="card-title grey-text text-darken-4">
                            10+
                          </h4>
                          <p className="medium-small grey-text">
                            Work Experience
                          </p>
                        </div>
                        <div className="col s2 center-align">
                          <h4 className="card-title grey-text text-darken-4">
                            6
                          </h4>
                          <p className="medium-small grey-text">
                            Completed Projects
                          </p>
                        </div>
                        <div className="col s2 center-align">
                          <h4 className="card-title grey-text text-darken-4">
                            $ 1,253,000
                          </h4>
                          <p className="medium-small grey-text">Busness Profit</p>
                        </div>
                        <div className="col s1 right-align">
                        <Link to="editprofile" className="btn-floating activator waves-effect waves-light darken-2 right"><Edit/></Link>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="profile-page-content" className="row">
                    <div id="profile-page-sidebar" className="col s12 m4">
                      <div className="card light-blue">
                        <div className="card-content white-text">
                          <span className="card-title">About Me!</span>
                          <p>
                            I am a very simple card. I am good at containing small
                            bits of information. I am convenient because I require
                            little markup to use effectively.
                          </p>
                        </div>
                      </div>
                      <ul
                        id="profile-page-about-details"
                        className="collection z-depth-1"
                      >
                        <li className="collection-item">
                          <div className="row">
                            <div className="col s5 grey-text darken-1">
                              <i className="mdi-action-wallet-travel" /> Project
                            </div>
                            <div className="col s7 grey-text text-darken-4 right-align">
                              ABC Name
                            </div>
                          </div>
                        </li>
                        <li className="collection-item">
                          <div className="row">
                            <div className="col s5 grey-text darken-1">
                              <i className="mdi-social-poll" /> Skills
                            </div>
                            <div className="col s7 grey-text text-darken-4 right-align">
                              HTML, CSS
                            </div>
                          </div>
                        </li>
                        <li className="collection-item">
                          <div className="row">
                            <div className="col s5 grey-text darken-1">
                              <i className="mdi-social-domain" /> Lives in
                            </div>
                            <div className="col s7 grey-text text-darken-4 right-align">
                              NY, USA
                            </div>
                          </div>
                        </li>
                        <li className="collection-item">
                          <div className="row">
                            <div className="col s5 grey-text darken-1">
                              <i className="mdi-social-cake" /> Birth date
                            </div>
                            <div className="col s7 grey-text text-darken-4 right-align">
                              18th June, 1991
                            </div>
                          </div>
                        </li>
                      </ul>
                      <div className="card amber darken-2">
                        <div className="card-content white-text center-align">
                          <p className="card-title">
                            <i className="mdi-social-group-add" /> 3685
                          </p>
                          <p>Followers</p>
                        </div>
                      </div>
                      <ul
                        id="profile-page-about-feed"
                        className="collection z-depth-1"
                      >
                        <li className="collection-item avatar">
                          <img
                            src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
                            alt=""
                            className="circle"
                          />
                          <span className="title">Project Title</span>
                          <p>
                            Task assigned to changes.
                            <br />{" "}
                            <span className="ultra-small">Second Line</span>
                          </p>
                          <a className="secondary-content">
                            <i className="mdi-action-grade" />
                          </a>
                        </li>
                        <li className="collection-item avatar">
                          <i className="mdi-file-folder circle" />
                          <span className="title">New Project</span>
                          <p>
                            First Line of Project Work
                            <br />{" "}
                            <span className="ultra-small">Second Line</span>
                          </p>
                          <a className="secondary-content">
                            <i className="mdi-social-domain" />
                          </a>
                        </li>
                        <li className="collection-item avatar">
                          <i className="mdi-action-assessment circle green" />
                          <span className="title">New Payment</span>
                          <p>
                            Last UK Project Payment
                            <br /> <span className="ultra-small">$ 3,684.00</span>
                          </p>
                          <a className="secondary-content">
                            <i className="mdi-editor-attach-money" />
                          </a>
                        </li>
                        <li className="collection-item avatar">
                          <i className="mdi-av-play-arrow circle red" />
                          <span className="title">Latest News</span>
                          <p>
                            company management news
                            <br />{" "}
                            <span className="ultra-small">Second Line</span>
                          </p>
                          <a className="secondary-content">
                            <i className="mdi-action-track-changes" />
                          </a>
                        </li>
                      </ul>
  
                      <ul id="task-card" className="collection with-header">
                        <li className="collection-header cyan">
                          <h4 className="task-card-title">My Task</h4>
                          <p className="task-card-date">March 26, 2015</p>
                        </li>
                      </ul>
                    </div>
                    <div id="profile-page-wall" className="col s12 m8">
                      <div id="profile-page-wall-share" className="row">
                        <div className="col s12">
                          <ul className="tabs tab-profile z-depth-1 light-blue">
                            <li className="tab col s3">
                              <a
                                className="white-text waves-effect waves-light active"
                                href="#UpdateStatus"
                              >
                                <i className="mdi-editor-border-color" /> Update
                                Status
                              </a>
                            </li>
                            <li className="tab col s3">
                              <a
                                className="white-text waves-effect waves-light"
                                href="#AddPhotos"
                              >
                                <i className="mdi-image-camera-alt" /> Add Photos
                              </a>
                            </li>
                            <li className="tab col s3">
                              <a
                                className="white-text waves-effect waves-light"
                                href="#CreateAlbum"
                              >
                                <i className="mdi-image-photo-album" /> Create
                                Album
                              </a>
                            </li>
                          </ul>
                          <div
                            id="UpdateStatus"
                            className="tab-content col s12  grey lighten-4"
                          >
                            <div className="row">
                              <div className="col s2">
                                <img
                                  src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
                                  alt=""
                                  className="circle responsive-img valign profile-image-post"
                                />
                              </div>
                              <div className="input-field col s10">
                                <textarea
                                  id="textarea"
                                  row="2"
                                  className="materialize-textarea"
                                />
                                <label>Whats on your mind?</label>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col s12 m6 share-icons">
                                <a>
                                  <i className="mdi-image-camera-alt" />
                                </a>
                                <a>
                                  <i className="mdi-action-account-circle" />
                                </a>
                                <a>
                                  <i className="mdi-hardware-keyboard-alt" />
                                </a>
                                <a>
                                  <i className="mdi-communication-location-on" />
                                </a>
                              </div>
                              <div className="col s12 m6 right-align">
                                <a
                                  className="dropdown-button btn"
                                  data-activates="profliePost"
                                >
                                  <i className="mdi-action-language" /> Public
                                </a>
                                <ul id="profliePost" className="dropdown-content">
                                  <li>
                                    <a>
                                      <i className="mdi-action-language" /> Public
                                    </a>
                                  </li>
                                  <li>
                                    <a>
                                      <i className="mdi-action-face-unlock" />{" "}
                                      Friends
                                    </a>
                                  </li>
                                  <li>
                                    <a>
                                      <i className="mdi-action-lock-outline" />{" "}
                                      Only Me
                                    </a>
                                  </li>
                                </ul>
  
                                <a className="waves-effect waves-light btn">
                                  <i className="mdi-maps-rate-review left" />Post
                                </a>
                              </div>
                            </div>
                          </div>
  
                          <div
                            id="AddPhotos"
                            className="tab-content col s12  grey lighten-4"
                          >
                            <div className="row">
                              <div className="col s2">
                                <img
                                  src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
                                  alt=""
                                  className="circle responsive-img valign profile-image-post"
                                />
                              </div>
                              <div className="input-field col s10">
                                <textarea
                                  id="textarea"
                                  row="2"
                                  className="materialize-textarea"
                                />
                                <label className="">
                                  Share your favorites photos!
                                </label>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col s12 m6 share-icons">
                                <a>
                                  <i className="mdi-image-camera-alt" />
                                </a>
                                <a>
                                  <i className="mdi-action-account-circle" />
                                </a>
                                <a>
                                  <i className="mdi-hardware-keyboard-alt" />
                                </a>
                                <a>
                                  <i className="mdi-communication-location-on" />
                                </a>
                              </div>
                              <div className="col s12 m6 right-align">
                                Trigger
                                <a
                                  className="dropdown-button btn"
                                  data-activates="profliePost2"
                                >
                                  <i className="mdi-action-language" /> Public
                                </a>
                                Structure
                                <ul
                                  id="profliePost2"
                                  className="dropdown-content"
                                >
                                  <li>
                                    <a>
                                      <i className="mdi-action-language" /> Public
                                    </a>
                                  </li>
                                  <li>
                                    <a>
                                      <i className="mdi-action-face-unlock" />{" "}
                                      Friends
                                    </a>
                                  </li>
                                  <li>
                                    <a>
                                      <i className="mdi-action-lock-outline" />{" "}
                                      Only Me
                                    </a>
                                  </li>
                                </ul>
                                <a className="waves-effect waves-light btn">
                                  <i className="mdi-maps-rate-review left" />Post
                                </a>
                              </div>
                            </div>
                          </div>
                          <div
                            id="CreateAlbum"
                            className="tab-content col s12  grey lighten-4"
                          >
                            <div className="row">
                              <div className="col s2">
                                <img
                                  src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
                                  alt=""
                                  className="circle responsive-img valign profile-image-post"
                                />
                              </div>
                              <div className="input-field col s10">
                                <textarea
                                  id="textarea"
                                  row="2"
                                  className="materialize-textarea"
                                />
                                <label className="">Create awesome album.</label>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col s12 m6 share-icons">
                                <a>
                                  <i className="mdi-image-camera-alt" />
                                </a>
                                <a>
                                  <i className="mdi-action-account-circle" />
                                </a>
                                <a>
                                  <i className="mdi-hardware-keyboard-alt" />
                                </a>
                                <a>
                                  <i className="mdi-communication-location-on" />
                                </a>
                              </div>
                              <div className="col s12 m6 right-align">
                                <a
                                  className="dropdown-button btn"
                                  data-activates="profliePost3"
                                >
                                  <i className="mdi-action-language" /> Public
                                </a>
                                <ul
                                  id="profliePost3"
                                  className="dropdown-content"
                                >
                                  <li>
                                    <a>
                                      <i className="mdi-action-language" /> Public
                                    </a>
                                  </li>
                                  <li>
                                    <a>
                                      <i className="mdi-action-face-unlock" />{" "}
                                      Friends
                                    </a>
                                  </li>
                                  <li>
                                    <a>
                                      <i className="mdi-action-lock-outline" />{" "}
                                      Only Me
                                    </a>
                                  </li>
                                </ul>
  
                                <a className="waves-effect waves-light btn">
                                  <i className="mdi-maps-rate-review left" />Post
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div id="profile-page-wall-posts" className="row">
                        <div className="col s12">
                          <div id="profile-page-wall-post" className="card">
                            <div className="card-profile-title">
                              <div className="row">
                                <div className="col s1">
                                  <img
                                    src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
                                    alt=""
                                    className="circle responsive-img valign profile-post-uer-image"
                                  />
                                </div>
                                <div className="col s10">
                                  <p className="grey-text text-darken-4 margin">
                                    John Doe
                                  </p>
                                  <span className="grey-text text-darken-1 ultra-small">
                                    Shared publicly - 26 Jun 2015
                                  </span>
                                </div>
                                <div className="col s1 right-align">
                                  <i className="mdi-navigation-expand-more" />
                                </div>
                              </div>
                              <div className="row">
                                <div className="col s12">
                                  <p>
                                    I am a very simple wall post. I am good at
                                    containing <a>#small</a> bits of{" "}
                                    <a>#information</a>. I require little more
                                    information to use effectively.
                                  </p>
                                </div>
                              </div>
                            </div>
                            <div className="card-content">
                              <p>
                                I am a very simple card. I am good at containing
                                small bits of information. I am convenient because
                                I require little markup to use effectively.
                              </p>
                            </div>
                            <div className="card-action row">
                              <div className="col s4 card-action-share">
                                <a>Like</a>
                                <a>Share</a>
                              </div>
  
                              <div className="input-field col s8 margin">
                                <input
                                  id="profile-comments"
                                  type="text"
                                  className="validate margin"
                                />
                                <label className="">Comments</label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          ) : (
            ""
          )}
        </div>
      );
    }
    
  }
}

function mapStateToProps(state) {
  return {
    user: state.LoginReducer.user
  };
}

export default connect(mapStateToProps)(Profile);
