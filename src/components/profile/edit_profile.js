import React, { Component } from "react";
import { connect } from "react-redux";
import * as loginAction from "../../actions/loginAction";
import { bindActionCreators } from "redux";
import {uploadPicture} from '../../actions/commonActions'
class EditProfile extends Component {
  constructor() {
    super();
    this.state = {
        user:null
    }
    this.handleFileUpload = this.handleFileUpload.bind(this);
  }

  componentWillMount(){
      if(this.props.user){
        this.setState({user:this.props.user.providerData[0]})
      }
  }

  componentWillReceiveProps(nextprops){
    if(nextprops.user){
        this.setState({user:nextprops.user.providerData[0]})
        }
}

handleChange = name => event => {
    this.setState({
        user: Object.assign({}, this.state.user, {
          [name]: event.target.value,
        })
      });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.actions.actionUser.updateUser(this.state.user).then(()=>{
        this.props.history.push("/profile");
    });
  };

  handleFileUpload(e) {
    
    uploadPicture(e.target.files[0]).then((image)=>{
        console.log(image)
        this.setState({
            user: Object.assign({}, this.state.user, {
              'photoURL': image.downloadURL
            })
          });
    })
  }

  render() {
    if(this.state.user){
        return (
            <div className="row">
              <form className="col s12" onSubmit={this.handleSubmit.bind(this)} encType="multipart/form-data">
                <div className="row">
                  <div className="input-field col s12">
                    <input id="displayName" type="text" className="validate" value={this.state.user.displayName==null?'':this.state.user.displayName} onChange={this.handleChange("displayName")}/>
                    <label className={this.state.user.displayName==null?'':'active'} >Username</label>
                  </div>
                </div>
                <div className="row">
                  <div className="input-field col s12">
                    <input id="email" type="email" className="validate" value={this.state.user.email} onChange={this.handleChange("email")} disabled/>
                    <label className="active">Email</label>
                  </div>
                </div>
                <div className="row">
                  <div className="input-field col s12">
                    <input id="image" type="file" className="validate" onChange={this.handleFileUpload}/>
                    <label className="active">Image</label>
                  </div>
                  {this.state.user.photoURL===null?'':<img src={this.state.user.photoURL} alt="profile_pic"/>}
                </div>
                <div className="row">
                <button type="submit">
                    Update
                    </button>
                </div>
              </form>
            </div>
          );
    }else{
        return (
        <div className="row">
            Loading
        </div>
        )
    }
    
  }
}

function mapDispatchToProps(dispatch) {
    return {
      actions: {
        actionUser: bindActionCreators(loginAction, dispatch)
      }
    };
  }

function mapStateToProps(state) {
    return {
      user: state.LoginReducer.user
    };
  }

export default connect(mapStateToProps,mapDispatchToProps)(EditProfile);
