import React, { Component } from "react";
import Button from "material-ui/Button";
import * as firebase from "firebase";
import { toast,ToastContainer } from 'react-toastify';

class Register extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  state = {
    email: "",
    password: ""
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    firebase
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(result => {
        this.props.history.push("/profile");
      })
      .catch(function(error) {
        toast.error(error.message);
      });
  };

  render() {
    return (
      <div className="row">
        <ToastContainer />
        <div className="col s12">
          <form
            noValidate
            autoComplete="off"
            onSubmit={this.handleSubmit.bind(this)}
            className="login-form"
          >
          <div className="row">
            <div className="input-field col s12 center">
              <p className="center login-form-text">Register</p>
            </div>
          </div>
          <div className="row margin">
            <div className="input-field col s12">
              <i className="material-icons prefix pt-5">person_outline</i>
              <input
                id="email"
                value={this.state.email}
                onChange={this.handleChange("email")}
                type="email"
              />
              <label className="center-align">Email</label>
            </div>
          </div>
          <div className="row margin">
            <div className="input-field col s12">
              <i className="material-icons prefix pt-5">lock_outline</i>
              <input
                id="password"
                value={this.state.password}
                onChange={this.handleChange("password")}
                type="password"
              />
              <label className="center-align">Password</label>
            </div>
          </div>
         
               
            <Button type="submit" raised>
              Sign Up
            </Button>
          </form>
        </div>
      </div>
    );
  }
}

export default Register;
