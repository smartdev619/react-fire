import { asyncActionNames} from '../GlobalActionCreators';

const loginUser = asyncActionNames('LOGIN_USER');

export function LoginReducer(state = { user: false }, action) {
  switch (action.type) {
    case loginUser.success:
      return { user: action.payload }
    default:
      return state
  }
}
